﻿namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter year when you were born: ");
            int year = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter month when you were born: ");
            int month = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter day when you were born: ");
            int day = Convert.ToInt32(Console.ReadLine());

            try
            {
                DateTime current = DateTime.Today;
                DateTime bithdate = new DateTime(current.Year, month, day);

                if (bithdate < current)
                {
                    bithdate = bithdate.AddYears(1);
                }

                TimeSpan next_birthday = bithdate - current;
                Console.WriteLine($"\nDays to your birthday: {next_birthday.Days}");
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("\nInvalid date");
            } 
        }
    }
}
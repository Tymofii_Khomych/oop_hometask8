﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your post: ");
            string user_input = Console.ReadLine();

            if (Enum.TryParse(user_input, true, out Post post))
            {
                Console.Write("Enter the number of working hours: ");
                int hours = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(Accountant.AskForBonus(post, hours) ?
                    "\nYou can ask for bonus" : "\nYou can not ask for bonus");
            }
            else
            {
                Console.WriteLine("\nInvalid post");
            }
        }
    }
}
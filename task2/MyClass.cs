﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    static class MyClass
    {
        public static void Print(string text, int color)
        {
            int size = Enum.GetValues(typeof(Colors)).Length;

            if (color < 0 || color > size)
            {
                Console.WriteLine("This color can't be found");
            }
            else
            {
                Colors UserColor = (Colors)color;
                Console.ForegroundColor = (ConsoleColor)UserColor;
                Console.WriteLine($"Your text with chosen color - {(Colors)color}: {text}");
                Console.ResetColor();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    enum Colors : byte
    {
        White,
        Red,
        Green,
        Blue,
        Yellow,
        Pink,
        Orange,
        Purple
    }
}

﻿namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input some text: ");
            string str = Console.ReadLine();
            Console.Write("Choose a color (0-7): ");
            int color = Convert.ToInt32(Console.ReadLine());

            MyClass.Print(str, color);
        }
    }
}